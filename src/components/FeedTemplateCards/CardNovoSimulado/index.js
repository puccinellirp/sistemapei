import React from 'react';

import { View, TouchableOpacity, Text } from 'react-native';
import Icon from "react-native-vector-icons/MaterialIcons";
import { Card } from 'react-native-shadow-cards';

import { Container, CardTitle, CardButton } from './styles';

const CardNovoSimulado = () => (
    <Container>
        <Card style={{ width: '100%', paddingTop: 10, paddingBottom: 10 }} cornerRadius={0} elevation={3}>
            <CardTitle><Icon
                style={{ textAlign: 'center' }}
                name="notifications-none"
                color="#fff"
                size={32}
            /><Text style={{ color: '#fff' }}>Novo Simulado Disponível</Text></CardTitle>
            <View style={{ paddingTop: 10, paddingBottom: 10, paddingRight: 40, paddingLeft: 40, }}>
                <Text>Prof. Eugênio Hordones</Text>
                <Text>Adicionou um novo simulado de Geografia</Text>
                <Text style={{ color: '#f46522' }}>Faça-o agora e ganhe XP para subir de nível e melhorar seu avatar!</Text>
            </View>
            <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                <Text style={{ marginRight: 10, fontSize: 12 }}>26/09/2019 às 11h10</Text>
                <TouchableOpacity style={{ width: '40%' }}>
                    <CardButton>Fazer Simulado</CardButton>
                </TouchableOpacity>
            </View>
        </Card>
    </Container>
);

export default CardNovoSimulado;
