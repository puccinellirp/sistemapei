
import styled from 'styled-components/native';

export const Container = styled.View`
  marginTop:15px;
  borderTopWidth: 2.5px;
  borderTopColor: #f46522
`;

export const CardTitle = styled.View`
    display: flex;
    flexDirection: row;
    justifyContent: space-around;
    alignItems: center;
    borderWidth: 2px;
    borderLeftWidth: 0px;
    borderColor: #0c1c3f;
    width: 67%;
    padding: 5px;
    borderTopRightRadius: 20px;
    borderBottomRightRadius: 20px;
    `;
export const CardButton = styled.Text`
  color: white;
  font-weight: bold;
  paddingTop: 5px;
  paddingBottom: 5px;
  textAlign: center;
  backgroundColor: #19ac2d;
  borderRadius: 30px;
`;