import React from 'react';

import { View, Text } from 'react-native';
import Icon from "react-native-vector-icons/MaterialIcons";
import { Card } from 'react-native-shadow-cards';

import { Container, CardTitle } from './styles';

const CardComunicado = () => (
    <Container>
        <Card style={{ width: '100%', paddingTop: 10, paddingBottom: 10, backgroundColor: '#f46522' }} cornerRadius={0} elevation={3}>
            <CardTitle><Icon
                style={{ textAlign: 'center' }}
                name="notifications-none"
                color="#f46522"
                size={32}
            /><Text style={{ color: '#f46522' }}>COMUNICADO!</Text></CardTitle>
            <View style={{ paddingTop: 10, paddingBottom: 5, paddingRight: 40, paddingLeft: 40, }}>
                <Text style={{ color: '#fff' }}>No dia 20 de novembro haverá uma palestra sobre Futuro e Carreira.</Text>
                <Text style={{ color: '#fff' }}>Não deixe de participar.</Text>
            </View>
            <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', marginLeft: 35 }}>
                <Text style={{ marginRight: 10, fontSize: 12, color: '#fff' }}>26/09/2019 às 11h10</Text>
            </View>
        </Card>
    </Container>
);

export default CardComunicado;
