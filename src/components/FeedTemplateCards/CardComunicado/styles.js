
import styled from 'styled-components/native';

export const Container = styled.View`
  marginTop:15px;
`;

export const CardTitle = styled.View`
    display: flex;
    flexDirection: row;
    justifyContent: space-around;
    alignItems: center;
    backgroundColor: #fff;
    width: 43%;
    padding: 5px;
    borderTopRightRadius: 20px;
    borderBottomRightRadius: 20px;
    `;