import React from 'react';

import { View, TouchableOpacity, Text, Image } from 'react-native';
import { Card } from 'react-native-shadow-cards';

import { CardTitle, CardButton } from './styles';

const CardNovoSimuladoNotif = (props) => (
    <Card style={{
        width: '47%', paddingTop: 10, paddingBottom: 10, marginTop: 15,
        borderTopWidth: 2.5,
        borderTopColor: '#f46522'
    }} cornerRadius={0} elevation={3}>
        <CardTitle><Image source={props.image} style={{ height: props.height, width: props.width }} /><Text style={{ fontSize: 13, padding: 12 }}>{props.classes}</Text></CardTitle>
        <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
            <TouchableOpacity style={{ width: '65%' }}>
                <CardButton>{props.subject}</CardButton>
            </TouchableOpacity>
        </View>
    </Card>
);

export default CardNovoSimuladoNotif;