import React from 'react';

import { View, Text, TouchableOpacity } from 'react-native';
import { Card } from 'react-native-shadow-cards';
import Icon from "react-native-vector-icons/MaterialIcons";
import { CardTitle, CardButton } from './styles';

const ProntoCard = () => (<Card style={{
    width: '48%', paddingTop: 10, paddingBottom: 10, marginTop: 15,
    borderTopWidth: 2.5,
    backgroundColor: '#fcfcfc',
    borderTopColor: '#f46522'
}} cornerRadius={0} elevation={3}>
    <View>
        <CardTitle style={{ backgroundColor: '#fff', width: '96%', justifyContent: 'flex-end', borderWidth: 1, borderColor: '#e0e0e0' }}>
            <Text style={{ color: '#19ac2d' }}>100xp</Text></CardTitle>
        <CardTitle style={{ position: 'absolute' }}>
            <Text style={{ color: '#fff' }}>Iniciativa</Text>
            <Icon
                name="notifications-none"
                color="#fff"
                size={20}
            /></CardTitle>
    </View>
    <View style={{ padding: 15 }}>
        <Text style={{ fontSize: 13 }}>Faça sua primeira aula</Text>
    </View>
    <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
        <TouchableOpacity style={{ width: '60%' }}>
            <CardButton>Resgatar</CardButton>
        </TouchableOpacity>
    </View>
</Card>);

export default ProntoCard;
