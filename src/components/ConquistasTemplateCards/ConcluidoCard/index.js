import React from 'react';

import { View, Text } from 'react-native';
import { Card } from 'react-native-shadow-cards';
import { CardTitle, CardButton } from './styles';

const ConcluidoCard = () => (
    <Card style={{
        width: '48%', paddingTop: 10, paddingBottom: 10, marginTop: 15,
        borderTopWidth: 2.5,
        backgroundColor: '#fcfcfc',
        borderTopColor: '#f46522'
    }} cornerRadius={0} elevation={3}>
        <View>
            <CardTitle style={{ backgroundColor: '#c2c2c2', width: '96%', justifyContent: 'flex-end' }}>
                <Text style={{ color: '#ffff' }}>300xp</Text></CardTitle>
            <CardTitle style={{ position: 'absolute', backgroundColor: '#ffff', borderWidth: 1, borderColor: '#e0e0e0' }}>
                <Text style={{ color: '#c2c2c2' }}>Calculo X</Text>
            </CardTitle>
        </View>
        <View style={{ padding: 15 }}>
            <Text style={{ fontSize: 13, color: '#c2c2c2' }}>Conclua o primeiro módulo de matemática</Text>
        </View>
        <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
            <CardButton style={{ backgroundColor: '#c2c2c2', width: '60%' }}>Resgatado</CardButton>
        </View>
    </Card>
);

export default ConcluidoCard;
