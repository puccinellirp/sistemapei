import styled from 'styled-components'

export const CardTitle = styled.View`
    display: flex;
    flexDirection: row;
    alignItems: center;
    width:60%;
    backgroundColor: #19ac2d;
    paddingHorizontal: 10;
    paddingVertical: 10;
    justifyContent: space-around;
    borderTopRightRadius: 30;
    borderBottomRightRadius: 30
    `;
export const CardButton = styled.Text`
  color: white;
  font-weight: bold;
  paddingTop: 1px;
  paddingBottom: 1px;
  textAlign: center;
  backgroundColor: #19ac2d;
  borderRadius: 30px;
`;