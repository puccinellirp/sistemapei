import React from 'react';

import { View, TouchableOpacity, Text } from 'react-native';
import Icon from "react-native-vector-icons/MaterialIcons";
import { Card } from 'react-native-shadow-cards';

import { CardTitle, CardButton } from './styles';

const CardNovasAulas = () => (
    <Card style={{
        width: '47%', paddingTop: 10, paddingBottom: 10, marginTop: 15,
        borderTopWidth: 2.5,
        borderTopColor: '#0c1c3f',
        backgroundColor: '#f46522'
    }} cornerRadius={0} elevation={3}>
        <CardTitle><Icon
            style={{
                textAlign: 'center', backgroundColor: '#0c1c3f',
                padding: 5,
                borderRadius: 20
            }}
            name="notifications-none"
            color="#fff"
            size={20}
        /><Text style={{ marginLeft: 5, fontSize: 10, color: '#fff' }}>26/09/2019 às 11h10</Text></CardTitle>
        <View style={{ padding: 15 }}>
            <Text style={{ fontSize: 13, color: '#fff' }}>Novas aulas de Ling. Portuguesa disponíveis!</Text>
        </View>
        <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
            <TouchableOpacity style={{ width: '65%' }}>
                <CardButton>Conferir</CardButton>
            </TouchableOpacity>
        </View>
    </Card>
);

export default CardNovasAulas;