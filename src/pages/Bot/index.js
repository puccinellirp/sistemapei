import React, { useState, useEffect } from 'react';

import Icon from "react-native-vector-icons/MaterialIcons";
import { Image, Text, View, StatusBar, TextInput, ImageBackground, TouchableOpacity, KeyboardAvoidingView, Platform } from 'react-native';
import { Card } from 'react-native-shadow-cards';
import bg from '../../assets/bg.png';
import bot from '../../assets/bot.png';
import { Header } from 'react-navigation-stack';
import { FlatList } from 'react-native-gesture-handler';
import signalr from 'react-native-signalr';


function Bot() {
    const [value, onChangeText] = useState('');
    const [data, setData] = useState([])
    const [chatId, setChatId] = useState('');
    const connection = signalr.hubConnection('https://channelwebsocket.suitesaas.com.br/signalr/hubs');
    connection.logging = true;

    const proxy = connection.createHubProxy('chatHub');

    useEffect(() => {
        //receives broadcast messages from a hub function, called "helloApp"
        proxy.on('notificacaoDelegate', (response) => {
            console.log('message-from-server', response);
            if (response.tipo == 'MENSAGEM_NOVA') {
                setData([...data, {
                    "idMensagem": response.idMensagem,
                    "idRemetente": response.idRemetente,
                    "mensagem": response.mensagem
                }])
            } else if (response.tipo == 'CHAT_NOVO') {
                setChatId(response.idChat)
            }
            //Here I could response by calling something else on the server...
        });

        // atempt connection, and handle errors 
        connection.start().done(() => {
            console.log('Now connected, connection ID=' + connection.id);

            proxy.invoke('criarChatAnonimo', { "idServico": "B9DB53B9-2F84-4646-8044-65E764822697", "idDestinatario": null, "nomeRemetente": "", "idtAtendimento": "20191025175838", "CodApi": "", "UUI": [] })
                .done((directResponse) => {
                    console.log('direct-response-from-server', directResponse);
                }).fail(() => {
                    console.warn('Something went wrong when calling server, it might not be up and running?')
                });

        }).fail(() => {
            console.log('Failed');
        });
        //connection-handling
        connection.connectionSlow(() => {
            console.log('We are currently experiencing difficulties with the connection.')
        });

        connection.error((error) => {
            const errorMessage = error.message;
            let detailedError = '';
            if (error.source && error.source._response) {
                detailedError = error.source._response;
            }
            if (detailedError === 'An SSL error has occurred and a secure connection to the server cannot be made.') {
                console.log('When using react-native-signalr on ios with http remember to enable http in App Transport Security https://github.com/olofd/react-native-signalr/issues/14')
            }
            console.debug('SignalR error: ' + errorMessage, detailedError)
        });

    }, [])

    function HandleSubmit() {
        connection.start().done(() => {
            proxy.invoke('enviarMensagemTexto', { idChat: chatId, mensagem: value })
                .done((directResponse) => {
                    console.log('direct-response-from-server', directResponse);
                }).fail(() => {
                    console.warn('Something went wrong when calling server, it might not be up and running?')
                });

        }),
            setData([...data, {
                "idMensagem": Math.floor(Math.random() * 101),
                "idRemetente": 'user',
                "mensagem": value
            }]),
            onChangeText('')
    }
    return (
        <>
            <StatusBar barStyle='dark-content' backgroundColor='#f2f7fc' translucent />
            <ImageBackground source={bg} style={{ position: "absolute", width: '100%', height: '100%' }} />
            <FlatList
                style={{ padding: 10 }}
                contentContainerStyle={{
                    paddingBottom: 100
                }}
                data={data}
                keyExtractor={data => String(data.idMensagem)}
                renderItem={
                    ({ item }) => item.idRemetente == '45515b08-6ec0-40ce-86b0-947c2adf9288' ? (
                        <View style={{ display: 'flex', flexDirection: 'row' }}>
                            <Image source={bot} style={{ width: 40, height: 36 }} />
                            <View style={{
                                alignSelf: 'flex-start', marginLeft: 5, marginBottom: 10, backgroundColor: '#fff', borderRadius: 10, borderTopLeftRadius: 0, padding: 10, maxWidth: '80%'
                            }}>
                                <Text>{String(item.mensagem)}</Text>
                            </View>

                        </View>
                    ) : (<View style={{
                        alignSelf: 'flex-end', marginRight: 3, marginBottom: 5, backgroundColor: '#f2f7fc', borderRadius: 10, borderBottomRightRadius: 0, padding: 10, maxWidth: '95%'
                    }}>
                        <Text>{String(item.mensagem)}</Text>
                    </View>)
                }
            />
            <KeyboardAvoidingView keyboardVerticalOffset={Platform.select({
                ios: () => 0,
                android: () => 80
            })()} behavior='padding' style={{ padding: 10, display: 'flex', flexDirection: 'row', position: 'absolute', bottom: 0 }}>
                <View style={{ padding: 10 }}>
                    <Card style={{ width: '100%', display: 'flex', flexDirection: 'row', alignItems: 'center' }} cornerRadius={100}>
                        <TextInput style={{ padding: 10, width: '100%' }} placeholder='Digite aqui...' onChangeText={text => onChangeText(text)}
                            value={value} />
                        {value == '' ? <TouchableOpacity style={{ position: 'absolute', right: 0, padding: 8 }}>
                            <Icon
                                name="mic"
                                color="#0c1c3f"
                                size={32}
                            />
                        </TouchableOpacity> : <TouchableOpacity style={{ position: 'absolute', right: 0, padding: 8 }} onPress={HandleSubmit}>
                                <Icon
                                    name="send"
                                    color="#0c1c3f"
                                    size={28}
                                />
                            </TouchableOpacity>}
                    </Card>
                </View>
            </KeyboardAvoidingView>
        </>
    )
}

export default Bot;
