import React, { useState } from 'react';
import Icon from "react-native-vector-icons/MaterialIcons";
import { View, StatusBar, Image } from 'react-native';
import { Badge } from 'react-native-elements';

import flag from '../../assets/flag.png';
import school from '../../assets/school.png';
import simulate from '../../assets/simulate.png';
import avatar from '../../assets/grupo.png';
import perfil from '../../assets/pedro.png';
import robot from '../../assets/Componente.png';


import { Container, ItemTitle, ItemRow, Item, Robot } from './styles';
function Menu(props) {
    const [notify, SetNotify] = useState(true);
    return (
        <>
            <StatusBar barStyle='light-content' backgroundColor='#f46522' translucent />
            <Container>
                <ItemRow>
                    <Item style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }} onPress={() => props.navigation.push('Feed')}>
                        <ItemTitle>Feed</ItemTitle>
                        <View>
                            <Icon
                                name="timeline"
                                color="#0c1c3f"
                                size={48}
                            />
                            {
                                notify ?
                                    <Badge
                                        status="error"
                                        containerStyle={{ position: 'absolute', top: 31, right: -12 }}
                                        value="Novo"
                                    /> : <View />
                            }
                        </View>
                    </Item>
                    <Item style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }} onPress={() => props.navigation.navigate('Notificacoes')}>
                        <ItemTitle>Notificações</ItemTitle>
                        <View>
                            <Icon
                                name="notifications-none"
                                color="#0c1c3f"
                                size={48}
                            />
                            {
                                notify ?
                                    <Badge
                                        status="error"
                                        containerStyle={{ position: 'absolute', top: 33, right: -3 }}
                                        value="1"
                                    /> : <View />
                            }
                        </View>
                    </Item>
                </ItemRow>
                <ItemRow>
                    <Item style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }} onPress={() => props.navigation.push('Trilha')} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                        <ItemTitle>Trilha</ItemTitle>
                        <Icon
                            name="play-circle-outline"
                            color="#0c1c3f"
                            size={48}
                        />
                        {
                            notify ?
                                <Badge
                                    status="error"
                                    containerStyle={{ position: 'absolute', top: 35, right: 2 }}
                                    value="1"
                                /> : <View />
                        }
                    </Item>
                    <Item style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }} onPress={() => props.navigation.push('Conquistas')}>
                        <ItemTitle>Conquistas</ItemTitle>
                        <Image source={flag} style={{ width: 40, height: 40 }} />
                        {
                            notify ?
                                <Badge
                                    status="error"
                                    containerStyle={{ position: 'absolute', top: 35, right: 5 }}
                                    value="99+"
                                /> : <View />
                        }
                    </Item>
                </ItemRow>
                <ItemRow style={{ marginRight: 20 }}>
                    <Item style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }} onPress={() => props.navigation.push('Simulados')} >
                        <ItemTitle>Simulados</ItemTitle>
                        <Image source={simulate} style={{ width: 40, height: 40 }} />
                        {
                            notify ?
                                <Badge
                                    status="error"
                                    containerStyle={{ position: 'absolute', top: 35, right: 5 }}
                                    value="99+"
                                /> : <View />
                        }
                    </Item>
                    <Item style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }} onPress={() => props.navigation.push('Aulas')}>
                        <ItemTitle>Aulas</ItemTitle>
                        <Image source={school} style={{ width: 40, height: 40 }} />
                        {
                            notify ?
                                <Badge
                                    status="error"
                                    containerStyle={{ position: 'absolute', top: 35, right: -10 }}
                                    value="99+"
                                /> : <View />
                        }
                    </Item>
                </ItemRow>
                <ItemRow>
                    <Item style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }} onPress={() => props.navigation.push('Avatar')}>
                        <ItemTitle>Avatar</ItemTitle>
                        <Image source={avatar} />
                        {
                            notify ?
                                <Badge
                                    status="error"
                                    containerStyle={{ position: 'absolute', top: 40, right: -10 }}
                                    value="99+"
                                /> : <View />
                        }
                    </Item>
                    <Item style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }} onPress={() => props.navigation.push('Perfil')} >
                        <ItemTitle>Perfil</ItemTitle>
                        <Image source={perfil} />
                        {
                            notify ?
                                <Badge
                                    status="error"
                                    containerStyle={{ position: 'absolute', top: 45, right: -10 }}
                                    value="99+"
                                /> : <View />
                        }
                    </Item>
                </ItemRow>
                <Robot onPress={() => props.navigation.push('Bot')}>
                    <Image source={robot} />
                </Robot>
            </Container>
        </>
    );

}


export default Menu;
