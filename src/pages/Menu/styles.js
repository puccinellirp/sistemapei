import styled from 'styled-components/native';

export const ItemTitle = styled.Text`

`

export const Container = styled.View`
    flex: 1;
    flexDirection: column;
    flexWrap: wrap;
    backgroundColor: #fff;
    justifyContent: center;
`

export const ItemRow = styled.View`
    display:flex;
    flexDirection: row;
    justifyContent: space-around;
    marginBottom: 10%;

`

export const RobotBubble = styled.Text`
    textAlign: center;
    color:#fff;
    backgroundColor: #f46522;
    padding: 10px;
    borderRadius: 8;
`

export const Chat = styled.View`
    display:flex;
    flexDirection: row;
    justifyContent: flex-end;
    alignItems: center;
`

export const Item = styled.TouchableOpacity`

`
export const Robot = styled.TouchableOpacity`
    marginLeft: 60%;
`