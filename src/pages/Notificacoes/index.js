import React from 'react';

import { View, TouchableOpacity, Image } from 'react-native';
import NotificationsItem from '../../Validators/NotificationsItem'

import { Chat } from './styles';
import robot from '../../assets/Componente.png'

const Notificacoes = (props) => (
    <>
        <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', flexWrap: 'wrap' }}>
            <NotificationsItem type='Novo-Simulado' teacher='Prof. Eugênio Hordones' text='Adicionou um novo simulado de Geografia' />
            <NotificationsItem type='Convide-Amigos' />
            <NotificationsItem type='Novo-Simulado' teacher='Prof. Eugênio Hordones' text='Adicionou um novo simulado de Matemática' />

            <NotificationsItem type='Novas-Aulas' />
        </View>
        <Chat>
            <TouchableOpacity onPress={() => props.navigation.push('Bot')}>
                <Image source={robot} />
            </TouchableOpacity>
        </Chat>
    </>
);

export default Notificacoes;
