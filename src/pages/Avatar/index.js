import React from 'react';

import { View, Image, Text, StatusBar } from 'react-native';

import bg from '../../assets/bgavatar.png'
// import { Container } from './styles';

const Avatar = (props) => (
    <>
        <StatusBar barStyle='dark-content' translucent />
        <Image source={bg} style={{ height: '100%', width: '100%', position: 'absolute' }} />
    </>
);

export default Avatar;
