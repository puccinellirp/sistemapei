import React from 'react';

import { View, Text, Image, TouchableOpacity } from 'react-native';

import profile from '../../assets/pedro.png'
import chevronRight from '../../assets/chevron_right.png'
import chevronLeft from '../../assets/chevron_left.png'
import robot from '../../assets/Componente.png'
import { Chat } from './styles';

const Perfil = (props) => (
    <>
        <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', padding: 20, borderBottomWidth: 1, borderBottomColor: '#f46522' }}>
            <Image source={profile} style={{ height: 70, width: 70 }} />
            <View>
                <Text style={{ fontSize: 15 }}>João Pedro Silva</Text>
                <Text style={{ fontSize: 13 }}>8º ano</Text>
                <Text style={{ fontSize: 12 }}>Colégio Gabarito</Text>
            </View>
            <View style={{ width: 1, height: '80%', backgroundColor: '#000' }} />
            <View>
                <Text style={{ fontSize: 15 }}>Ranking Geral</Text>
                <Text style={{ fontSize: 13 }}>227º</Text>
            </View>
        </View>
        <View style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', padding: 15 }}>
            <Text style={{ color: '#f46522', fontSize: 15, marginBottom: 10 }}> Indicação para o Vestibular</Text>
            <Text style={{ fontSize: 15, marginBottom: 10 }}> Curso indicado baseado em suas notas atuais</Text>
            <View style={{ backgroundColor: '#0c1c3f', padding: 5, width: '40%', display: 'flex', alignItems: 'center', marginBottom: 10 }}>
                <Text style={{ color: '#fff', fontSize: 15 }}> Geografia</Text>
            </View>
            <Text style={{ fontSize: 12, marginBottom: 10 }}>Você está entre os 50 melhores em Geografia na sua escola</Text>
        </View>
        <View style={{ backgroundColor: '#f46522', padding: 10, display: 'flex', alignItems: 'center', width: '100%' }}>
            <Text style={{ color: '#fff' }}>Para que você gostaria de prestar o vestibular?</Text>
        </View>
        <View style={{ backgroundColor: '#0c1c3f', display: 'flex', alignItems: 'center', padding: 20, paddingTop: 0 }}>
            <Text style={{ color: '#fff', fontSize: 10 }}>Cálculo baseado em seus simulados e as notas de corte do último Enem</Text>
            <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', width: '100%', padding: 10 }}>
                <TouchableOpacity>
                    <Image source={chevronLeft} style={{ height: 12, width: 12 }} />
                </TouchableOpacity>
                <Text style={{ color: '#fff' }}>MEDICINA</Text>
                <TouchableOpacity>
                    <Image source={chevronRight} style={{ height: 12, width: 12 }} />
                </TouchableOpacity>
            </View>
            <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', width: '100%' }}>
                <View style={{ width: '60%' }}>
                    <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', borderBottomColor: '#f46522', borderBottomWidth: 2, marginBottom: '40%' }}>
                        <Text style={{ color: '#fff' }}>nota de corte</Text>
                        <Text style={{ color: '#fff' }}>950pts</Text>
                    </View>
                    <View style={{ display: 'flex', flexDirection: 'row', borderBottomWidth: 2, borderBottomColor: '#fff', position: 'absolute', bottom: 0, right: 0, width: '50%' }}>
                        <View style={{ width: 6, borderRadius: 30, height: 65, backgroundColor: '#19ac2d', position: 'absolute', bottom: 0, marginLeft: 10 }} />
                        <View style={{ width: 6, borderRadius: 30, height: 55, backgroundColor: '#19aca7', position: 'absolute', bottom: 0, marginLeft: 20 }} />
                        {/* height:85 = 100% */}
                    </View>
                </View>
                <View>
                    <Text style={{ color: '#fff', backgroundColor: '#19ac2d', padding: 5, borderRadius: 30, textAlign: 'center', width: '50%', marginBottom: 5 }}>Você</Text>
                    <Text style={{ color: '#fff', backgroundColor: '#19aca7', padding: 5, borderRadius: 30, textAlign: 'center' }}>Média da escola</Text>
                </View>
            </View>
            <View style={{ borderBottomColor: '#fff', borderBottomWidth: 2, paddingBottom: 8, marginTop: 10 }}>
                <Text style={{ color: '#fff' }}>Você está entre os 200 melhores na sua escola</Text>
            </View>
            <View style={{ paddingTop: 10 }}>
                <Text style={{ color: '#fff', fontSize: 13, backgroundColor: '#f46522', padding: 3, borderRadius: 30, textAlign: 'center' }}>Para atingir a nota de corte você precisa estudar mais</Text>
                <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', padding: 5, marginTop: 10 }}>
                    <TouchableOpacity style={{ width: '30%' }}>
                        <Text style={{ color: '#fff', backgroundColor: '#19ac2d', padding: 5, borderRadius: 30, textAlign: 'center' }}>Biologia</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ width: '30%' }}>
                        <Text style={{ color: '#fff', backgroundColor: '#19ac2d', padding: 5, borderRadius: 30, textAlign: 'center' }}>Química</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ width: '30%' }}>
                        <Text style={{ color: '#fff', backgroundColor: '#19ac2d', padding: 5, borderRadius: 30, textAlign: 'center' }}>Matemática</Text>
                    </TouchableOpacity>
                </View>
                <Text style={{ color: '#fff', fontSize: 12, textAlign: 'center', marginTop: 10 }}>Clique na disciplina e tenha um material pronto para você</Text>
            </View>
        </View>
        <Chat>
            <TouchableOpacity onPress={() => props.navigation.push('Bot')}>
                <Image source={robot} />
            </TouchableOpacity>
        </Chat>
    </>
);

export default Perfil;
