import React from 'react';

import { ScrollView, TouchableOpacity, Image } from 'react-native';
import AulasCard from '../../components/AulasCard'

import robot from '../../assets/Componente.png'

import matematica from '../../assets/matematica.png'
import portugues from '../../assets/portugues.png'
import biologia from '../../assets/biologia.png'
import quimica from '../../assets/quimica.png'
import fisica from '../../assets/fisica.png'
import artes from '../../assets/artes.png'
import historia from '../../assets/historia.png'
import filosofia from '../../assets/filosofia.png'
import sociologia from '../../assets/sociologia.png'
import ingles from '../../assets/ingles.png'

import { Chat } from './styles';

const Aulas = (props) => (
    <>
        <ScrollView contentContainerStyle={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', flexWrap: 'wrap', paddingBottom: 10 }}>
            <AulasCard classes='10 aulas disponíveis' subject='Matemática' image={matematica} height={40} width={40} />
            <AulasCard classes='10 aulas disponíveis' subject='Ling. Portuguesa' image={portugues} height={40} width={50} />
            <AulasCard classes='10 aulas disponíveis' subject='Biologia' image={biologia} height={42} width={38} />
            <AulasCard classes='10 aulas disponíveis' subject='Química' image={quimica} height={37} width={35} />
            <AulasCard classes='10 aulas disponíveis' subject='Física' image={fisica} height={42} width={38} />
            <AulasCard classes='10 aulas disponíveis' subject='Artes' image={artes} height={37} width={38} />
            <AulasCard classes='10 aulas disponíveis' subject='História' image={historia} height={40} width={40} />
            <AulasCard classes='10 aulas disponíveis' subject='Filosofia' image={filosofia} height={46} width={40} />
            <AulasCard classes='10 aulas disponíveis' subject='Sociologia' image={sociologia} height={52} width={50} />
            <AulasCard classes='10 aulas disponíveis' subject='Inglês' image={ingles} height={40} width={50} />
        </ScrollView>
        <Chat>
            <TouchableOpacity onPress={() => props.navigation.push('Bot')}>
                <Image source={robot} />
            </TouchableOpacity>
        </Chat>
    </>
);

export default Aulas;
