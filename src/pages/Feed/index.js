import React from 'react';
import { ScrollView, StatusBar, Image, TouchableOpacity } from 'react-native';
import FeedItem from '../../Validators/FeedItem'
import robot from '../../assets/Componente.png'
import { Chat } from './styles';

function Feed(props) {
    return (
        <>
            <StatusBar barStyle='dark-content' backgroundColor='#ffffff' translucent />
            <ScrollView>
                <FeedItem type='Novo-Simulado' />
                <FeedItem type='Iniciou-Simulado' />
                <FeedItem type='Comunicado' />
                <FeedItem type='Novo-Desafio' />
                <FeedItem type='Cumpriu-Desafio' />

                <FeedItem type='Novo-Simulado' />
                <FeedItem type='Iniciou-Simulado' />
                <FeedItem type='Comunicado' />
                <FeedItem type='Novo-Desafio' />
                <FeedItem type='Cumpriu-Desafio' />
            </ScrollView>
            <Chat>
                <TouchableOpacity onPress={() => props.navigation.push('Bot')}>
                    <Image source={robot} />
                </TouchableOpacity>
            </Chat>
        </>
    )
}

export default Feed;
