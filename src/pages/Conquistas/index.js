import React from 'react';

import { View, Text, Image, ScrollView, TouchableOpacity } from 'react-native';
import ConquistaItem from '../../Validators/ConquistaItem';
import { Chat } from './styles'

import robot from '../../assets/Componente.png'
import flag from '../../assets/green_flag.png';

const Conquistas = (props) => (
    <>
        <View style={{ paddingVertical: 10, display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }} >
            <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', marginLeft: 10 }}>
                <Image source={flag} style={{ height: 25, width: 25 }} />
                <Text style={{ fontSize: 20, margin: 5, color: '#19ac2d' }}>Conquistas</Text>
            </View>
            <View style={{ marginRight: 10, width: '30%' }}>
                <Text style={{ color: '#19ac2d', textAlign: 'right' }}>27/100</Text>
                <View>
                    <View style={{ backgroundColor: '#e0e0e0', width: '100%', height: 3 }} />
                    <View style={{ backgroundColor: '#19ac2d', width: '27%', height: 3, position: 'absolute' }} />
                </View>
            </View>
        </View>
        <ScrollView contentContainerStyle={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', flexWrap: 'wrap', paddingBottom: 10 }}>
            <ConquistaItem type='Pronto' />
            <ConquistaItem type='Pronto' />
            <ConquistaItem type='Pronto' />
            <ConquistaItem type='Pronto' />
            <ConquistaItem type='Pronto' />
            <ConquistaItem type='Em-Progresso' />
            <ConquistaItem type='Em-Progresso' />
            <ConquistaItem type='Em-Progresso' />
            <ConquistaItem type='Concluido' />
            <ConquistaItem type='Concluido' />
            <ConquistaItem type='Concluido' />
            <ConquistaItem type='Concluido' />
            <ConquistaItem type='Concluido' />
            <ConquistaItem type='Concluido' />
            <ConquistaItem type='Concluido' />
            <ConquistaItem type='Concluido' />
        </ScrollView>
        <Chat>
            <TouchableOpacity onPress={() => props.navigation.push('Bot')}>
                <Image source={robot} />
            </TouchableOpacity>
        </Chat>
    </>
);

export default Conquistas;
