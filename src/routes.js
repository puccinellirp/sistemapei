import React from 'react';
import { Image, Platform, StatusBar } from 'react-native';
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'

import logo from './assets/gabarito_logo.png';

import Menu from './pages/Menu';
import Feed from './pages/Feed';
import Notificacoes from './pages/Notificacoes';
import Trilha from './pages/Trilha';
import Conquistas from './pages/Conquistas';
import Simulados from './pages/Simulados';
import Aulas from './pages/Aulas';
import Avatar from './pages/Avatar';
import Perfil from './pages/Perfil';
import Bot from './pages/Bot';

const Routes = createAppContainer(
    createStackNavigator({
        Bot: {
            screen: Bot,
            navigationOptions: () => ({
                headerTitle: 'PEI',
                headerStyle: {
                    backgroundColor: '#ffff'
                }
            })
        },
        Menu: {
            screen: Menu,
            navigationOptions: () => ({
                headerTitle: <Image source={logo} />,
                headerStyle: {
                    backgroundColor: '#f46522'
                }
            })
        },
        Feed: {
            screen: Feed,
            navigationOptions: () => ({
                headerTitle: 'Feed',
                headerStyle: {
                    backgroundColor: '#ffffff'
                }
            })
        },
        Notificacoes: {
            screen: Notificacoes,
            navigationOptions: () => ({
                headerTitle: 'Notificações',
                headerStyle: {
                    backgroundColor: '#ffffff'
                }
            })
        },
        Trilha: {
            screen: Trilha,
            navigationOptions: () => ({
                headerTitle: 'Trilha',
                headerStyle: {
                    backgroundColor: '#ffffff'
                }
            })
        },
        Conquistas: {
            screen: Conquistas,
            navigationOptions: () => ({
                headerTitle: 'Conquistas',
                headerStyle: {
                    backgroundColor: '#ffffff'
                }
            })
        },
        Simulados: {
            screen: Simulados,
            navigationOptions: () => ({
                headerTitle: 'Simulados',
                headerStyle: {
                    backgroundColor: '#ffffff'
                }
            })
        },
        Aulas: {
            screen: Aulas,
            navigationOptions: () => ({
                headerTitle: 'Aulas',
                headerStyle: {
                    backgroundColor: '#ffffff'
                }
            })
        },
        Avatar: {
            screen: Avatar,
            navigationOptions: () => ({
                headerTitle: 'Avatar',
                headerStyle: {
                    backgroundColor: '#ffffff'
                }
            })
        },
        Perfil: {
            screen: Perfil,
            navigationOptions: () => ({
                headerTitle: 'Perfil',
                headerStyle: {
                    backgroundColor: '#ffffff'
                }
            })
        },
    }, {
        headerLayoutPreset: 'center',
        initialRouteName: 'Bot',
    },
        {
            cardStyle: {
                paddingTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight
            }
        }
    ));

export default Routes;