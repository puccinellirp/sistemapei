import React from 'react';

import { View, Text } from 'react-native';
import CardNovoSimulado from '../../components/FeedTemplateCards/CardNovoSimulado';
import CardIniciouSimulado from '../../components/FeedTemplateCards/CardIniciouSimulado';
import CardComunicado from '../../components/FeedTemplateCards/CardComunicado';
import CardNovoDesafio from '../../components/FeedTemplateCards/CardNovoDesafio';
import CardCumpriuDesafio from '../../components/FeedTemplateCards/CardCumpriuDesafio';

function FeedItem(props) {
    let type = props.type;
    switch (type) {
        case 'Novo-Simulado':
            return (
                <CardNovoSimulado />
            )
        case 'Iniciou-Simulado':
            return (
                <CardIniciouSimulado />
            )
        case 'Comunicado':
            return (
                <CardComunicado />
            )
        case 'Novo-Desafio':
            return (
                <CardNovoDesafio />
            )
        case 'Cumpriu-Desafio':
            return (
                <CardCumpriuDesafio />
            )
        default:
            return (
                <View><Text>Por Favor digite um tipo de Card válido!</Text></View>
            )
    }
}

export default FeedItem;
